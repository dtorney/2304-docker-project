FROM python:alpine
WORKDIR /app
COPY . /app
EXPOSE 80
RUN apk --no-cache add curl
HEALTHCHECK --interval=1s --timeout=5s --retries=3 \
    CMD curl -f http://localhost:5000/health || exit 1
RUN pip install -r requirements.txt
ENV FLASK_APP /src/app.py
ENV FLASK_ENV development
CMD ["flask", "run", "--host=0.0.0.0"]
