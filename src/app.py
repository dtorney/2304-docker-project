from os import environ
from flask import Flask, jsonify, request, make_response
import json
from datetime import datetime
import pymongo
from bson.objectid import ObjectId
from prometheus_flask_exporter import PrometheusMetrics


if 'DB_SECRET_FILE' in environ:
    with open(str(environ.get('DB_SECRET_FILE')), 'r') as secret_file:
        data = secret_file.read()
    obj = json.loads(data)
    host = str(obj['host'])
    mongoClient = pymongo.MongoClient(host)
else:
    mongoClient = pymongo.MongoClient("mongodb://mongo:27017",
                                      serverSelectionTimeoutMS=1)

mongoCollection = mongoClient["cs2304"]["blabs"]

app = Flask(__name__)

environ["DEBUG_METRICS"] = "true"

metrics = PrometheusMetrics(app)

metrics.info('app_info', 'Application info', version='1.0.0')


@app.route('/health')
def get_health():
    try:
        mongoClient.server_info()
    except Exception:
        return make_response(jsonify("No MongoDB connection"), 404)
    return make_response(jsonify("Successful MongoDB connection"), 200)


@app.route('/blabs')
def get_blabs():
    blabs = []
    for blab in mongoCollection.find():
        b = blab.copy()
        b["id"] = str(blab["_id"])
        del b["_id"]
        blabs.append(b)

    return jsonify(blabs)


@app.route('/blabs', methods=['POST'])
@metrics.counter('blabs', 'Number of blabs')
def add_blab():
    newBlab = request.json
    newBlab["postTime"] = datetime.now()
    response = mongoCollection.insert_one(newBlab)
    result = jsonify({"id": str(response.inserted_id), "postTime": newBlab
                     ["postTime"], "author": newBlab["author"], "message":
                      newBlab["message"]})
    return result


@app.route('/blabs/<id>', methods=['DELETE'])
def delete_blab(id):
    for blab in mongoCollection.find():
        b = blab.copy()
        if str(b["_id"]) == id:
            mongoCollection.delete_one({"_id": ObjectId(str(id))})
            return jsonify("Blab deleted successfully")
    return make_response(jsonify("Blab not found"), 404)
